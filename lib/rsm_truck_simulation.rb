require "rsm_truck_simulation/version"

module RsmTruckSimulation
  class Simulator
    require 'httparty'

    def initialize(url)
      @api_base = url
      setup_simulation
    end

    def start
      @devices.each do |device|
        start_truck_journey(device["id"])
        sleep 30 # Wait before starting the next truck
      end
    end

    private

    def setup_simulation
      # Create a Company
      company = create_company

      # Create a Plant
      plant_site = create_plant(company)

      # Create a Jobsite
      @chestnut_hill_site = create_jobsite(company)

      # Create an Order
      @order = create_order(job_site=@chestnut_hill_site)

      # Create Vehicles
      @trucks = create_trucks(home_plant=plant_site)

      # Create Devices
      @devices = create_devices

      # Assign each Device to a Truck
      assign_devices_to_trucks

      # Saugus to Chestnut Hill route sequential co-ords array.
      get_route_coordinates

      @devices.each { |device| instance_variable_set("@device_#{device['id'].gsub('-', '_')}_is_en_route", true) }
    end

    def create_company
      response = HTTParty.post("#{@api_base}/companies", {
        :headers => { 'Content-Type': 'application/json'},
        :body => {
          "name": "G&C",
          "address_line_1": "1200 Mainland Road",
          "address_line_2": "Little Hampton",
          "city": "Boston",
          "state": "MA",
          "zip": "90210",
          "phone_number": "18001234567"
        }.to_json
      })
      company = JSON.parse(response.body)
    end

    def create_plant(company)
      response = HTTParty.post("#{@api_base}/sites", {
        :headers => { 'Content-Type': 'application/json'},
        :body => {
          "latitude": "42.4467838",
          "longitude": "-71.027534",
          "range": "100",
          "name": "Saugus",
          "company_id": company["id"],
          "site_type": "plant"
        }.to_json
      })
      saugus_site = JSON.parse(response.body)
    end

    def create_jobsite(company)
      response = HTTParty.post("#{@api_base}/sites", {
        :headers => { 'Content-Type': 'application/json'},
        :body => {
          "latitude": "42.3361588",
          "longitude": "-71.1635649",
          "range": "100",
          "name": "Chestnut Hill",
          "company_id": company["id"],
          "site_type": "construction_site"
        }.to_json
      })
      @chestnut_hill_site = JSON.parse(response.body)
    end

    def create_trucks(home_plant)
      trucks = []
      1.upto(3) do |i|
        response = HTTParty.post("#{@api_base}/vehicles", {
          :headers => { 'Content-Type': 'application/json'},
          :body => {
            "number": "Truck#{i.to_s}",
            "home_plant_site_id": home_plant["id"]
          }.to_json
        })
        trucks << JSON.parse(response.body)
      end

      trucks
    end

    def create_devices
      devices = []
      1.upto(3) do
        response = HTTParty.post("#{@api_base}/devices", {
          :headers => { 'Content-Type': 'application/json'},
          :body => {
            "country_code": "+44",
            "number": "7#{rand(10 ** 10).to_s}"
          }.to_json
        })
        devices << JSON.parse(response.body)
      end
      devices
    end

    def assign_devices_to_trucks
      @trucks.each_with_index do |truck, index|
        response = HTTParty.post("#{@api_base}/vehicles/#{truck["id"]}/device", {
          :headers => { 'Content-Type': 'application/json'},
          :body => {
            "device_id": @devices[index]["id"]
          }.to_json
        })
      end
    end

    def get_route_coordinates
      response = HTTParty.get("https://route.cit.api.here.com/routing/7.2/calculateroute.json?app_id=riVkzmDkOqhTcHp7HbF9&app_code=vDV9C7nf2qe7hCwbHPpRqA&waypoint0=geo!42.447643,-71.027382;100&waypoint1=geo!42.335438,-71.162983;100&mode=fastest;car;traffic:enabled&routeattributes=shape")
      @coords_array = JSON.parse(response.body)["response"]["route"].first["shape"]
    end

    def start_truck_journey(device_id)
      Thread.new do # Start a new truck journey in a new thread so they can run concurrently.
        p "Start journey for Device #{device_id}"
        while true do
          if instance_variable_get("@device_#{device_id.gsub('-', '_')}_is_en_route") == true
            # If the truck is en route to the jobsite, go forwards along the route
            0.upto(@coords_array.length) { |i| send_location_update(i, device_id) }
          else
            # If the truck is returning to the plant, go backwards along the route
            @coords_array.length.downto(0) { |i| send_location_update(i, device_id) }
          end
        end
      end
    end

    def send_location_update(i, device_id)
      return if @coords_array[i].nil? || i % 5 != 0 # Use only every fifth coordinate so we don't send tons of updates.
      coordinate_array = @coords_array[i].split(',')

      if coordinate_array[i] == coordinate_array.first
        # If the index matches the first coordinate in the array, give the Vehicle a new Payload and set it to be en_route
        p "Vehicle is now en route."
        instance_variable_set("@device_#{device_id.gsub('-', '_')}_is_en_route", true)
      elsif coordinate_array[i] == coordinate_array.last
        # If the index matches the last coordinate in the array, the truck is returning to the plant.
        p "Vehicle is now returning to the plant."
        instance_variable_set("@device_#{device_id.gsub('-', '_')}_is_en_route", false)
      end

      body = {
        "latitude": coordinate_array[0],
        "longitude": coordinate_array[1]
      }
      p "Sending update: #{body.to_json}"
      response = HTTParty.put("#{@api_base}/devices/#{device_id}/location-update", {
        :headers => { 'Content-Type': 'application/json'},
        :body => body.to_json
      })

      sleep 1 # Wait 1s before sending the next location update.
    end

  end
end
